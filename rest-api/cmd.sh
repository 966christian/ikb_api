# mvn install:install-file -Dfile="src/main/resources/jconn4.jar" -DgroupId=com.sybase -DartifactId=jconn4 -Dversion=4_RELEASE -Dpackaging=jar

export PORT_SERVER='9090'
export ACTIVE_PROFILE='dev'
# export DB_HOST='127.0.0.1'
export DB_HOST='10.201.60.17'
# export DB_PORT='3306'
# export DB_PORT='3307'
export DB_PORT='2638'
# export DB_NAME='db_rest'
export DB_NAME='dbh2h'
# export DB_USER='root'
export DB_USER='mit_user'
export DB_PASS='mit_pass'
# export DB_PASS=
echo "================================="
echo "============ SETTING ============"
echo "================================="
echo "PORT_SERVER = "${PORT_SERVER}
echo "ACTIVE_PROFILE = "${ACTIVE_PROFILE}
echo "DB_HOST = "${DB_HOST}
echo "DB_PORT = "${DB_PORT}
echo "DB_NAME = "${DB_NAME}
echo "DB_USER = "${DB_USER}
echo "DB_PASS = "${DB_PASS}
