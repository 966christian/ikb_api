/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.restapi.resources;

import com.restapi.models.Payment;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ahmad
 */
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Integer> {

    @Query(value = "CALL DBA.WS_ITC_PAYMENT(:prm1,:prm2);", nativeQuery = true)
    List<Payment> findData(@Param("prm1") Integer prm1, @Param("prm2") Integer prm2);
}
