package com.restapi.controllers.v1;

import com.restapi.models.Payment;
import com.restapi.services.PaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import com.restapi.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import com.restapi.utils.Response;

@Api(description = "Supports GET operation", tags = {"payment"})
@RestController
@RequestMapping(PaymentController.BASH_URL)
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    public static final String BASH_URL = "/api/v1/payment";

    @ApiOperation(value = "Lists all the payment", notes = "")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> findData(@RequestParam("prm1") Integer prm1, @RequestParam("prm2") Integer prm2) {
      try {
        Object data = paymentService.findData(prm1, prm2);
        if (data == null || data.toString() == "[]"){
          return new Response().response_json("data not found",null,HttpStatus.NO_CONTENT);
        }
    		return new Response().response_json("Success",data,HttpStatus.OK);
    	}catch(Exception ex) {
        throw new ResourceNotFoundException();
    	}
    }

}
