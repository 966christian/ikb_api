package com.restapi.models;


import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Category model class
 */
@Data
@Entity
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String DTB_SEQNO;
    private String DTP_NOPEL;
    private String DTP_KDUNIT;
    private String DTP_CUSTOMER;
    private String DTP_THNBLN;
    private String DTP_TAGIHAN;
    private String DTP_BAYAR;
    private String DTP_NOKARTU;
    private String DTP_PROCODE;
    private String DTP_STAN;
    private String DTP_LOCTIME;
    private String DTP_LOCDATE;
    private String DTP_STMDATE;
    private String DTP_MERCTYPE;
    private String DTP_INSCODE;
    private String DTP_REFFNO;
    private String DTP_TERMID;
    private String DTP_TYPE;
    private String DTP_REVERSE;
    private String DTP_NOTTS;
    private String DTP_SOURCE;
    private String DTP_BILLER;

}
