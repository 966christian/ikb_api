package com.restapi.services;

import com.restapi.models.Payment;
import com.restapi.resources.PaymentRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;

    public List<Payment> findData(Integer prm1, Integer prm2) {
        return paymentRepository.findData(prm1, prm2);
    }
}
