package com.restapi.utils;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import lombok.Data;
import java.time.LocalDateTime;

@Data
public class Response {
	private String code = HttpStatus.OK.toString();
	private HttpStatus status = HttpStatus.OK;
	private String message = null;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
	private LocalDateTime timestamp = LocalDateTime.now();
	private Object data = null;

	public ResponseEntity<?> response_json (String message, Object data, HttpStatus status){
		this.message = message;
		this.data = data;
		this.status = status;
		this.code = status.toString();
		return new ResponseEntity<Response>(this,HttpStatus.OK);
	}
}
